package app;
public interface CRC16CalculatorInterface {
    int Calculate(byte[] buffer);
}
