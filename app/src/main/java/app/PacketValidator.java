package app;
import java.nio.ByteBuffer;

public class PacketValidator implements PacketValidatorInterface {
    private CRC16CalculatorInterface crc16Calculator;

    public PacketValidator(CRC16CalculatorInterface crc16Calculator) {
        this.crc16Calculator = crc16Calculator;
    }

    // you can change this method to return boolean instead of throwing an exception. but change interface respectively, then
    @Override
    public void Validate(byte[] packet) throws Exception {
        ByteBuffer bf = ByteBuffer.wrap(packet);

        byte[] packetBeginning = new byte[13]; //first 13 bytes of the packet
        bf.get(packetBeginning, 0, 13);

        // calculate CRC16 of first 13 bytes
        int packetBeginningCrc = this.crc16Calculator.Calculate(packetBeginning);

        // get CRC16 that we received with the packet
        int bSrc = bf.getShort(14);

        // check if CRC16's are equal
        if(bSrc != packetBeginningCrc) {
            throw new Exception("packet checksum mismatch");
            // or return false; //if you don't want to mess with exceptions
        }

        // get message length
        int wLen = bf.getInt(10);

        // get actual message
        byte[] actualMessage = new byte[] {};
        bf.get(actualMessage, 16, wLen);

        // calculate CRC16 of the message
        int actualMessageCrc = this.crc16Calculator.Calculate(actualMessage);

        // get message CRC16 from the packet
        int bSrc2 = bf.getShort(16 + wLen - 1);

        // check if CRC16's are equal
        if(bSrc != actualMessageCrc) {
            throw new Exception("packet message checksum mismatch");
            // or return false; //if you don't want to mess with exceptions
        }
        // return true;
    }
}
