
package app;
import java.util.Arrays;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;
import java.util.Random;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.ArrayList;
public class PacketGenerator implements PacketGeneratorInterface {
    private CRC16Calculator crcCalculator;

    public PacketGenerator(CRC16Calculator crcCalculator) {
        this.crcCalculator = crcCalculator;
    }


    public byte[] GeneratePacket(byte bMagic, byte bSrc, long bPktId, int cType, int bUserId, byte[] message) {

        message = cypherMessage(message);
   
        ByteBuffer bf = ByteBuffer.allocate(30 + message.length);

        // bf.put((byte)0x13); 
        bf.put(bMagic).put(bSrc);

        // mesage number. it should be incrementable i think, but i generate it randomly
        bf.putLong(bPktId);

        // message length
        bf.putInt(message.length);

        // get first 13 bytes of the packet
        byte[] packetBeginning = new byte[13];

        bf.duplicate().rewind().get(packetBeginning, 0, 13);
        // calculate CRC16
        short packetBeginningCrc = (short) this.crcCalculator.Calculate(packetBeginning);

        // save CRC16 so it can be validated by client
        bf.putShort(packetBeginningCrc);

        bf.putInt(cType);
        bf.putInt(bUserId);
        // message
        bf.put(message);

        byte[] messageBytes = Arrays.copyOfRange(bf.array(), 16, 16+ message.length);

        // calculate CRC of message.
        int messageCrc = this.crcCalculator.Calculate(messageBytes);

        // save CRC of the message so it can be validated by client
        bf.putInt(messageCrc);

        // return an array
        return bf.array();
    }

    private static byte[] cypherMessage(byte[] message) {
//get key from file
        String encodedKey = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("key.txt")));
            encodedKey = br.readLine();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // decode the base64 encoded string to get a key
        byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
        SecretKey secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");

        //encrypt the message
        Cipher cipher;
        byte[] encrypted = null;
        try {
            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            encrypted = cipher.doFinal(message);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            return message;
        }

        return encrypted;
    }

}