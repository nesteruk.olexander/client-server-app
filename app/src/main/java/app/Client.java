package app;
public class Client implements ClientInterface {
    private PacketValidatorInterface packetValidator;
    public Client(PacketValidatorInterface packetValidator) {
        this.packetValidator = packetValidator;
    }

    @Override
    public void Receive(byte[] packet) {
        try {
            this.packetValidator.Validate(packet);

            // do some stuff with the packet. decode it or something, i dunno
        }
        catch(Exception ex) {
            // do some stuff
        }
    }
}
