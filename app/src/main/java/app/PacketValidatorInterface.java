package app;
public interface PacketValidatorInterface {
    void Validate(byte[] packet) throws Exception;
}
