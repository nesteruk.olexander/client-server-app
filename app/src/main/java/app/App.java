package app;

import java.util.Random;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
        PacketGeneratorInterface generator = new PacketGenerator(new CRC16Calculator());
        PacketValidatorInterface validator = new PacketValidator(new CRC16Calculator());
        Random random = new Random();

        byte[] message = new byte[20];
        random.nextBytes(message);

        byte[] packet = generator.GeneratePacket((byte)0x13, (byte)random.nextInt(), random.nextLong(), random.nextInt(), random.nextInt(), message);
        
        validator.Validate(packet);
    }
}
