package app;

public interface PacketGeneratorInterface {
    byte[] GeneratePacket(byte bMagic, byte bSrc, long bPktId, int cType, int bUserId, byte[] message);
}